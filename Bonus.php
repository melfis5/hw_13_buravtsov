<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 11.02.2019
 * Time: 19:48
 */

class Bonus implements Writable
{
    protected $title;
    protected $type;
    protected $description;


    public function getSummaryLine()
    {
        return 'Title: ' . $this->title . ' Type: ' . $this->type . ' Description: ' . $this->description;
    }
}