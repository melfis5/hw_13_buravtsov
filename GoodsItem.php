<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 11.02.2019
 * Time: 19:42
 */

class GoodsItem extends Item
{
    protected $discount;

    //todo статический метод, возвращает строку - 'goods'
    public static function getType()
    {
        return 'goods';
    }

    //todo возвращает (цена - скидка)
    public function getPrice()
    {
        return $this->price -= $this->discount;
    }
}