<?php

abstract class Item implements Writable
{
    protected $title;
    protected $type;
    protected $price;

    /**
     * Item constructor.
     * @param $title
     * @param $type
     * @param $price
     */
    public function __construct($title, $price)
    {
        $this->title = $title;
        $this->price = $price;
    }


    //todo геттер для title
    public function getTitle()
    {
        return $this->title;
    }

    //todo возвращает строку base_item
    public static function getType()
    {
        return 'base_item';
    }

    public function getSummaryLine()
    {
        return 'Title: ' . $this->title . ' Price: ' . $this->price . ' Type: ' . self::getType();
    }

    abstract public function getPrice();
}