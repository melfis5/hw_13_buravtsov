<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 11.02.2019
 * Time: 19:50
 */

class ItemsWriter
{
    protected $item;

    public function addItem(Writable $item)
    {
        $this->item = $item;
    }

    //todo foreach создаст строку из возвращаемых значений метода getSummaryLine
    public function write()
    {
        $html = '';

        foreach ($this->item as $item){
            $html .= '<p>' . $item->getSummaryLine() . '</p>';
        }

        return $html;
    }
}